# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""


import Tkinter
import paramiko
import sys


commComm = 'comm::comm send [list '
hostName = sys.argv[1]        # Emulator IP Address : '192.168.233.133'
sgwPort = sys.argv[2]         # SGW Port : 5678
userName = 'root'             # Emulator username : root
passWord = 'polaris'          # Emulator password : polaris
emulatorStartIp = sys.argv[3] # Emulator Start IP Address : '192.168.233.100'



#Format : python    FileName.py     emulator_management_IP     SGW Port    emulator_start_IP

#----------- SSH Login to Emulator and starting up Nettest Server -------------
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostName, username=userName, password=passWord)

client.exec_command('cd /opt/polaris-nettest/server/; ./stop.sh')
stdin, stdout, stderr = client.exec_command('cd /opt/polaris-nettest/server/; ./start.sh -sgw '+sgwPort)
for line in stdout:
    print (line.strip('\n'))


client.close()




#--------------------- Assigning IP Addresses to all Nodes ----------------------
emulatorStartIp = emulatorStartIp.split('.')
baseIp = emulatorStartIp[0]+'.'+emulatorStartIp[1]+'.'+emulatorStartIp[2]+'.'
startIp = int(emulatorStartIp[3])
sgwIp = baseIp+str(startIp)
startIp+=1
sgwPcrfIp = baseIp+str(startIp)
startIp+=1



#------------------API Calls to Emulator Server for Configuration--------------------
tkinterobj=Tkinter.Tk()
tkinterobj.tk.eval('source comm-package.tcl')


command = ['PNReset',
            '{PNConnect -statusUpdateHook :emulator:1 -statusUpdatePort 49453 -reset 0}',
            '{PNConfigureEventLogging -level Information -compression 0 -enableDevLog 0 -repetition 1 -size 20 -maxFileSize 10 -compressedFileCount 50}',
            '{PNCreateNetwork -plmn TestNetwork -mcc 001 -mnc 01}',
            '{PNSGWConfigureServiceRestorationTimer -plmn TestNetwork -timeout 54}',
            '{PNSGWConfigureServiceRestorationRule -plmn TestNetwork -apn apn.testnetwork1 -qci 7 -priorityLevel 6}']

for i in range(0,len(command)):
  tkinterobj.tk.eval(commComm+str(sgwPort)+' '+hostName+'] '+command[i])
 
sgwHandle = tkinterobj.tk.eval(commComm+str(sgwPort)+' '+hostName+'] '+'{PNSGWCreate -plmn TestNetwork -intfS11S4c ens33 -ipaddrS11S4c '+sgwIp+' -netmaskS11S4c 24 -portS11S4c 2123 -intfS1S4S12u ens33 -ipaddrS1S4S12u '+sgwIp+' -netmaskS1S4S12u 24 -portS1S4S12u 2152 -intfS5S8c ens33 -ipaddrS5S8c '+sgwIp+' -netmaskS5S8c 24 -portS5S8c 2123 -intfS5S8u ens33 -ipaddrS5S8u '+sgwIp+' -netmaskS5S8u 24 -portS5S8u 2152 -intfGxc ens33 -ipaddrGxc '+sgwIp+' -netmaskGxc 24 -portGxc 3868 -intfS11u ens33 -ipaddrS11u '+sgwIp+' -netmaskS11u 24 -portS11u 2152 -diaHost "sgw1.localhost" -diaRealm "epc.mnc001.mcc001.3gppnetwork.org" -partialPathFailure 1 -pgwRestart 1 -modifyAccessBearer 1 -diaProxyPA2 0 -serviceRestoration 1 -nodeIdType 0 -nodeIdValue '+sgwIp+' -preferIPv4Address 1}')

tkinterobj.tk.eval(commComm+str(sgwPort)+' '+hostName+'] '+'{PNSGWConfigureDiameterAVPValidation -sgw '+str(sgwHandle)+' -isFailedAvpEnabled 0}')


tkinterobj.tk.eval(commComm+str(sgwPort)+' '+hostName+'] '+'{PNSGWConfigureNeighbourPCRF -sgw '+str(sgwHandle)+' -ipAddress '+sgwPcrfIp+' -diaPort 3868  -isDefault 1 -trnsprtProtoType 6 -nodeType 1 -realm epc.mnc001.mcc001.3gppnetwork.org}')




