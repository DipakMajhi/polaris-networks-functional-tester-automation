# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""




import paramiko
import sys

hostname = sys.argv[1]
port = sys.argv[2]
username = 'root'
password = 'polaris'

client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname, username=username, password=password)

#./start.sh -mme 5676
stdin, stdout, stderr = client.exec_command('cd /opt/polaris-nettest/server/; tclsh NetTest-Server.tcl -type MME -port 5676')
for line in stdout:
    print line.strip('\n')
    
client.close()

