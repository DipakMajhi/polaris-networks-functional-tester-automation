# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""



import time
import subprocess
import sys

hostname = sys.argv[1] # Emulator IP Address : '192.168.233.133'
port = sys.argv[2]     # Emulator Port : 5676
start_ip = sys.argv[3] # Emulator Start IP Address : '192.168.233.100'




#-----------Starting PolarisFT License Server-------------------
#subprocess.Popen(["service","polaris-license-server", "start"])


#------------------Starting PolarisFT Server---------------------
subprocess.Popen(["systemctl","stop", "polarisftd"])
subprocess.Popen(["systemctl","start", "polarisftd"])
time.sleep(2)
print (subprocess.Popen(["systemctl","status", "polarisftd"], stdout=subprocess.PIPE).communicate()[0])

#subprocess.Popen(["polarismmeft","-setup"])



#-------------------------------------------Executing Batch-1---------------------------------------------------

print ("\n\t\t --- : Executing Batch-1 ( Same IP - IPv4 ) : --- \n\n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch1SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/MME/Batch-1/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)



#----------------Emulator (MME) Configuration File---------------
#print (subprocess.Popen(["polarismmeft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-MME-Batch-1-SameIp-Ipv4.py",hostname,port,start_ip])


#----------------PolarisFT (MME) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-MME-Batch-1-SameIp-Ipv4.py",start_ip])



#2 left out due to ceash
TC = ['1', '3', '4', '5', '6', '7', '8', '10', '11', '13', '16', '18', '20', '21', '23', '24', '26']

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarismmeft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch1SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)








#-------------------------------------------Executing Batch-2---------------------------------------------------
print ("\n\t\t --- : Executing Batch-2 ( Same IP - IPv4 ) : --- \n\n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch2SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/MME/Batch-2/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)


#----------------Emulator (MME) Configuration File---------------
#print (subprocess.Popen(["polarismmeft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-MME-Batch-2-SameIp-Ipv4.py",hostname,port,start_ip])


#----------------PolarisFT (MME) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-MME-Batch-2-SameIp-Ipv4.py",start_ip])


TC = ['2.2.7', '2.2.8', '14.1', '14.2', '14.4', '14.6', '14.8.1', '14.8.3', '15.1', '15.2', '15.3.1', '15.3.4', '15.3.5', '15.3.7', '15.3.10', '15.3.12', '15.3.13', '15.3.17', '15.3.21', '15.3.22', '15.4.8' '19', '26.6.2.6.1.3']

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarismmeft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch2SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)






#-------------------------------------------Executing Batch-3---------------------------------------------------
print ("\n\t\t --- : Executing Batch-3 ( Same IP - IPv4 ) : --- \n\n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch3SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/MME/Batch-3/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)


#----------------Emulator (MME) Configuration File---------------
#print (subprocess.Popen(["polarismmeft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-MME-Batch-3-SameIp-Ipv4.py",hostname,port,start_ip])


#----------------PolarisFT (MME) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-MME-Batch-3-SameIp-Ipv4.py",start_ip])


TC = ['14.5', '14.7', '15.3', '19.1.2.2', '19.1.2.4', '26.6.2.6.1.4']

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarismmeft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch3SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)







#-------------------------------------------Executing Batch-4---------------------------------------------------
print ("\n\t\t --- : Executing Batch-4 ( Same IP - IPv4 ) : --- \n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch4SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/MME/Batch-4/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)


#----------------Emulator (MME) Configuration File---------------
#print (subprocess.Popen(["polarismmeft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-MME-Batch-4-SameIp-Ipv4.py",hostname,port,start_ip])


#----------------PolarisFT (MME) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-MME-Batch-4-SameIp-Ipv4.py",start_ip])


TC = ['15.4']

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarismmeft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch4SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)






#-------------------------------------------Executing Batch-5---------------------------------------------------
print ("\n\t\t --- : Executing Batch-5 ( Same IP - IPv4 ) : --- \n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch5SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/MME/Batch-5/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)


#----------------Emulator (MME) Configuration File---------------
#print (subprocess.Popen(["polarismmeft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-MME-Batch-5-SameIp-Ipv4.py",hostname,port,start_ip])


#----------------PolarisFT (MME) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-MME-Batch-5-SameIp-Ipv4.py",start_ip])


TC = ['14.8.2', '14.8.5.1', '14.8.6.1', '14.7.8', '14.7.9', '14.7.10', '14.5.6', '14.5.7', '14.5.8']

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarismmeft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch5SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)











#-------------------------------------------Executing Batch-6---------------------------------------------------
print ("\n\t\t --- : Executing Batch-6 ( Same IP - IPv4 ) : --- \n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch6SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/MME/Batch-6/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)


#----------------Emulator (MME) Configuration File---------------
#print (subprocess.Popen(["polarismmeft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-MME-Batch-6-SameIp-Ipv4.py",hostname,port,start_ip])


#----------------PolarisFT (MME) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-MME-Batch-6-SameIp-Ipv4.py",start_ip])


TC = ['2.2', '6.2', '9', '17', '23.1']

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarismmeft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch6SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)








