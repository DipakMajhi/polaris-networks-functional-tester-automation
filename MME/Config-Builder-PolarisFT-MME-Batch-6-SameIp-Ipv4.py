# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""


from xml.etree import ElementTree as ET
import sys


def General(Entity_Configuration, DUT_List):
    
    position_count = [position_count for position_count,position in enumerate(DUT_List) if position == 'General']
    test_General = Entity_Configuration[0].getchildren()[position_count[0]]
    test_General[0].attrib['Value'] = "MME-1"
    test_General[1].attrib['Value'] = "36412"
    test_General[2].attrib['Value'] = "29168"
    test_General[3].attrib['Value'] = "9082"
    test_General[4].attrib['Value'] = "2"
    test_General[5].attrib['Value'] = "1"
    test_General[6].attrib['Value'] = "Always"
    test_General[7].attrib['Value'] = "Before_S1-Setup_Procedure"
    test_General[8].attrib['Value'] = "Before_S1-Setup_Procedure"
    test_General[9].attrib['Value'] = "YES"
    test_General[10].attrib['Value'] = "NO"
    test_General[11].attrib['Value'] = "V9.x"
    test_General[12].attrib['Value'] = "IMSI-Required-Authentication-optional"
    test_General[13].attrib['Value'] = "NO"
    test_General[14].attrib['Value'] = "YES"
    
    

def Interfaces(Entity_Configuration, DUT_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(DUT_List) if position == 'Interfaces']
    IPv4 = (Entity_Configuration[0].getchildren()[position_count[0]]).getchildren()[0]
    IPv6 = (Entity_Configuration[0].getchildren()[position_count[0]]).getchildren()[1]
    
    #Configuration : IPv4
    IPv4[0].attrib['Value'] = "MME"
    IPv4[1].attrib['Value'] = "S1-MME IP Address"
    IPv4[2].attrib['Value'] = Base_IP+str(Start_IP)
    IPv4[3].attrib['Value'] = "NO"
    ipv4_ip_count = 4
    while ipv4_ip_count!=15:
        #Start_IP+=1
        IPv4[ipv4_ip_count].attrib['Value'] = Base_IP+str(Start_IP)
        ipv4_ip_count +=1
    
    '''
    #Configuration : IPv6
    ipv6_ip_count = 0
    while ipv6_ip_count!=12:
        #Start_IP+=1
        IPv6[ipv6_ip_count].attrib['Value'] = Base_IP+str(Start_IP)
        ipv6_ip_count +=1
    ''' 
    Start_IP+=1
    return Start_IP



def Home_PLMN(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Home PLMN']
    home_PLMN = Entity_Configuration[position_count[0]].getchildren()
    home_PLMN[0].attrib['Value'] = "TestNetwork"
    home_PLMN[1].attrib['Value'] = "001"
    home_PLMN[2].attrib['Value'] = "01"




def Visited_PLMN(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Visited PLMN']
    visited_PLMN = Entity_Configuration[position_count[0]].getchildren()
    visited_PLMN[0].attrib['Value'] = "Vodafone"
    visited_PLMN[1].attrib['Value'] = "404"
    visited_PLMN[2].attrib['Value'] = "30"
    



def Unknown_PLMN(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Unknown PLMN']
    unknown_PLMN = Entity_Configuration[position_count[0]].getchildren()
    unknown_PLMN[0].attrib['Value'] = "BSNL"
    unknown_PLMN[1].attrib['Value'] = "111"
    unknown_PLMN[2].attrib['Value'] = "222"
    



 
def Test_MME(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test MME']
    test_MME = Entity_Configuration[position_count[0]].getchildren()
    test_MME[0].attrib['Value'] = "NO"
    test_MME[1].attrib['Value'] = "Ethernet"
    test_MME[2].attrib['Value'] = "S10"
    test_MME[3].attrib['Value'] = "NO"
    test_MME[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_MME[5].attrib['Value'] = IPv4_Mask
    test_MME[6].attrib['Value'] = IPv4_Interface
    test_MME[7].attrib['Value'] = "::"
    test_MME[8].attrib['Value'] = "0"
    test_MME[9].attrib['Value'] = "lo"
    test_MME[10].attrib['Value'] = "2123"
    test_MME[11].attrib['Value'] = "MME-1"
    test_MME[12].attrib['Value'] = "2"
    test_MME[13].attrib['Value'] = "32768"
    test_MME[14].attrib['Value'] = "00000000"
    Start_IP+=1
    return Start_IP
    



def Test_UE(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test UE']
    test_UE = Entity_Configuration[position_count[0]].getchildren()
    test_UE[0].attrib['Value'] = "9990000001"
    test_UE[1].attrib['Value'] = "919293949596979"
    test_UE[2].attrib['Value'] = "Home-PLMN"
    test_UE[3].attrib['Value'] = "10"
    test_UE[4].attrib['Value'] = "00000000000000001234567800000001"
    test_UE[5].attrib['Value'] = "OP"
    test_UE[6].attrib['Value'] = "63bfa50ee6523365ff14c1f45f88737d"
    test_UE[7].attrib['Value'] = "EIA1"
    test_UE[8].attrib['Value'] = "EEA0"
    test_UE[9].attrib['Value'] = "Computed"
    test_UE[10].attrib['Value'] = "958801974f84733F"
    test_UE[11].attrib['Value'] = "Computed"
    test_UE[12].attrib['Value'] = "c5a1844f8754d6ad3707989fe15dcac3"
    test_UE[13].attrib['Value'] = "Computed"
    test_UE[14].attrib['Value'] = "b7bdb4e352520f44ca043ffb96e8fe2f"
    test_UE[15].attrib['Value'] = "Computed"
    test_UE[16].attrib['Value'] = "111d7ff18a4f64019a13e58c3ccc8aab0ffbdc28e861cd907b76150eb37d6a1a"
    test_UE[17].attrib['Value'] = "Computed"
    test_UE[18].attrib['Value'] = "35258B137f12a817437e7654fd13ec9b1358b9333c45b9044297f5661131712f"
    test_UE[19].attrib['Value'] = "Computed"
    test_UE[20].attrib['Value'] = "c73034e4ddf3c623b05568296a24b5741667383102dac3f529ccc2c88a7eb3ce"
    test_UE[21].attrib['Value'] = "c112345678"
    



def Test_Unknown_UE(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Unknown UE']
    unknown_UE = Entity_Configuration[position_count[0]].getchildren()
    unknown_UE[0].attrib['Value'] = "991111111"
    unknown_UE[1].attrib['Value'] = "919293949596979"
    unknown_UE[2].attrib['Value'] = "10"




def Test_eNodeB(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test eNodeB']
    test_eNodeB = Entity_Configuration[position_count[0]].getchildren()
    test_eNodeB[0].attrib['Value'] = "SCTP"
    test_eNodeB[1].attrib['Value'] = "S1-MME"
    test_eNodeB[2].attrib['Value'] = "NO"
    test_eNodeB[3].attrib['Value'] = Base_IP+str(Start_IP)
    test_eNodeB[4].attrib['Value'] = IPv4_Mask
    test_eNodeB[5].attrib['Value'] = IPv4_Interface
    test_eNodeB[6].attrib['Value'] = "::"
    test_eNodeB[7].attrib['Value'] = "0"
    test_eNodeB[8].attrib['Value'] = "lo"
    test_eNodeB[9].attrib['Value'] = "Test eNodeB"
    test_eNodeB[10].attrib['Value'] = "50"
    test_eNodeB[11].attrib['Value'] = "Home/HeNBGW"
    test_eNodeB[12].attrib['Value'] = "1"
    test_eNodeB[13].attrib['Value'] = "100"
    test_eNodeB[14].attrib['Value'] = "YES"
    Start_IP+=1
    return Start_IP



def Target_eNodeB(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Handover Target eNodeB']
    target_eNodeB = Entity_Configuration[position_count[0]].getchildren()
    target_eNodeB[0].attrib['Value'] = "SCTP"
    target_eNodeB[1].attrib['Value'] = "S1-MME"
    target_eNodeB[2].attrib['Value'] = "NO"
    target_eNodeB[3].attrib['Value'] = Base_IP+str(Start_IP)
    target_eNodeB[4].attrib['Value'] = IPv4_Mask
    target_eNodeB[5].attrib['Value'] = IPv4_Interface
    target_eNodeB[6].attrib['Value'] = "::"
    target_eNodeB[7].attrib['Value'] = "0"
    target_eNodeB[8].attrib['Value'] = "lo"
    target_eNodeB[9].attrib['Value'] = "Target eNodeB"
    target_eNodeB[10].attrib['Value'] = "101"
    target_eNodeB[11].attrib['Value'] = "Home/HeNBGW"
    target_eNodeB[12].attrib['Value'] = "2"
    target_eNodeB[13].attrib['Value'] = "101"
    Start_IP+=1
    return Start_IP



def Test_HSS(Entity_Configuration, Entity_List, Start_IP):
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test HSS']
    test_HSS = Entity_Configuration[position_count[0]].getchildren()
    test_HSS[0].attrib['Value'] = "YES"
    test_HSS[1].attrib['Value'] = "S6a"
    test_HSS[2].attrib['Value'] = "3868"
    test_HSS[3].attrib['Value'] = "NO"
    test_HSS[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_HSS[5].attrib['Value'] = IPv4_Mask
    test_HSS[6].attrib['Value'] = IPv4_Interface
    test_HSS[7].attrib['Value'] = "::"
    test_HSS[8].attrib['Value'] = "0"
    test_HSS[9].attrib['Value'] = "lo"
    test_HSS[10].attrib['Value'] = "SCTP"
    test_HSS[11].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_HSS[12].attrib['Value'] = "hss.epc.mnc001.mcc001.3gppnetwork.org"
    test_HSS[13].attrib['Value'] = "6"
    test_HSS[14].attrib['Value'] = "6"
    test_HSS[15].attrib['Value'] = "88000000001"
    test_HSS[16].attrib['Value'] = "99000000001"
    test_HSS[17].attrib['Value'] = "10.10.10.1"
    test_HSS[18].attrib['Value'] = "IPv4"
    test_HSS[19].attrib['Value'] = "apn.polarisnetworks.net"
    test_HSS[20].attrib['Value'] = "apn.polarisnetworks1.net"
    ##Configuration : Test PGW
    Start_IP+=1
    test_HSS[21].attrib['Value'] = Base_IP+str(Start_IP)
    test_HSS[22].attrib['Value'] = "0"
    test_HSS[23].attrib['Value'] = "1"
    test_HSS[24].attrib['Value'] = "Packet-and-Circuit"
    test_HSS[25].attrib['Value'] = "Local-Breakout"
    test_HSS[26].attrib['Value'] = "All-Allowed"
    test_HSS[27].attrib['Value'] = "117"
    test_HSS[28].attrib['Value'] = "100.100.100.123"
    Start_IP+=1
    return Start_IP



def Test_SGW(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test SGW']
    test_SGW = Entity_Configuration[position_count[0]].getchildren()
    test_SGW[0].attrib['Value'] = "Ethernet"
    test_SGW[1].attrib['Value'] = "YES"
    test_SGW[2].attrib['Value'] = "S11"
    test_SGW[3].attrib['Value'] = "2123"
    test_SGW[4].attrib['Value'] = "GTPC"
    test_SGW[5].attrib['Value'] = "NO"
    test_SGW[6].attrib['Value'] = Base_IP+str(Start_IP)
    test_SGW[7].attrib['Value'] = IPv4_Mask
    test_SGW[8].attrib['Value'] = IPv4_Interface
    test_SGW[9].attrib['Value'] = "::"
    test_SGW[10].attrib['Value'] = "0"
    test_SGW[11].attrib['Value'] = "lo"
    Start_IP+=1
    return Start_IP
    

def Target_SGW(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Handover Target SGW']
    target_SGW = Entity_Configuration[position_count[0]].getchildren()
    target_SGW[0].attrib['Value'] = "Ethernet"
    target_SGW[1].attrib['Value'] = "YES"
    target_SGW[2].attrib['Value'] = "S11"
    target_SGW[3].attrib['Value'] = "GTPC"
    target_SGW[4].attrib['Value'] = "NO"
    target_SGW[5].attrib['Value'] = Base_IP+str(Start_IP)
    target_SGW[6].attrib['Value'] = IPv4_Mask
    target_SGW[7].attrib['Value'] = IPv4_Interface
    target_SGW[8].attrib['Value'] = "::"
    target_SGW[9].attrib['Value'] = "0"
    target_SGW[10].attrib['Value'] = "lo"
    Start_IP+=1
    return Start_IP



def Test_CBC(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test CBC']
    test_CBC = Entity_Configuration[position_count[0]].getchildren()
    test_CBC[0].attrib['Value'] = "SCTP"
    test_CBC[1].attrib['Value'] = "NO"
    test_CBC[2].attrib['Value'] = "SBc"
    test_CBC[3].attrib['Value'] = "NO"
    test_CBC[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_CBC[5].attrib['Value'] = IPv4_Mask
    test_CBC[6].attrib['Value'] = IPv4_Interface
    test_CBC[7].attrib['Value'] = "::"
    test_CBC[8].attrib['Value'] = "0"
    test_CBC[9].attrib['Value'] = "lo"
    test_CBC[10].attrib['Value'] = "10"
    test_CBC[11].attrib['Value'] = "1"
    test_CBC[12].attrib['Value'] = "0"
    test_CBC[13].attrib['Value'] = "3000,2000,1000"
    test_CBC[14].attrib['Value'] = "TAI-List"
    test_CBC[15].attrib['Value'] = "10"
    test_CBC[16].attrib['Value'] = "Earthquake"
    test_CBC[17].attrib['Value'] = "Information"
    test_CBC[18].attrib['Value'] = "This is a Test Warning"
    test_CBC[19].attrib['Value'] = "English"
    test_CBC[20].attrib['Value'] = "0"
    Start_IP+=1
    return Start_IP



def Test_SGSN(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test SGSN']
    test_SGSN = Entity_Configuration[position_count[0]].getchildren()
    test_SGSN[0].attrib['Value'] = "Ethernet"
    test_SGSN[1].attrib['Value'] = "NO"
    test_SGSN[2].attrib['Value'] = "S3"
    test_SGSN[3].attrib['Value'] = "NO"
    test_SGSN[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_SGSN[5].attrib['Value'] = IPv4_Mask
    test_SGSN[6].attrib['Value'] = IPv4_Interface
    test_SGSN[7].attrib['Value'] = "::"
    test_SGSN[8].attrib['Value'] = "0"
    test_SGSN[9].attrib['Value'] = "lo"
    test_SGSN[10].attrib['Value'] = "2123"
    test_SGSN[11].attrib['Value'] = "1"
    test_SGSN[12].attrib['Value'] = "1"
    test_SGSN[13].attrib['Value'] = "100"
    test_SGSN[14].attrib['Value'] = "151"
    test_SGSN[15].attrib['Value'] = "21"
    Start_IP+=1
    return Start_IP


def Test_VLR(Entity_Configuration, Entity_List, Start_IP):
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test VLR']
    test_VLR = Entity_Configuration[position_count[0]].getchildren()
    test_VLR[0].attrib['Value'] = "NO"
    test_VLR[1].attrib['Value'] = "CSFB-Supported"
    test_VLR[2].attrib['Value'] = "SGs"
    test_VLR[3].attrib['Value'] = "NO"
    test_VLR[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_VLR[5].attrib['Value'] = IPv4_Mask
    test_VLR[6].attrib['Value'] = IPv4_Interface
    test_VLR[7].attrib['Value'] = "::"
    test_VLR[8].attrib['Value'] = "0"
    test_VLR[9].attrib['Value'] = "lo"
    test_VLR[10].attrib['Value'] = "29118"
    test_VLR[11].attrib['Value'] = "SCTP"
    test_VLR[12].attrib['Value'] = "1"
    Start_IP+=1
    return Start_IP


def Target_VLR(Entity_Configuration, Entity_List, Start_IP):
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Target VLR']
    target_VLR = Entity_Configuration[position_count[0]].getchildren()
    target_VLR[0].attrib['Value'] = "NO"
    target_VLR[1].attrib['Value'] = "SGs"
    target_VLR[2].attrib['Value'] = "NO"
    target_VLR[3].attrib['Value'] = Base_IP+str(Start_IP)
    target_VLR[4].attrib['Value'] = IPv4_Mask
    target_VLR[5].attrib['Value'] = IPv4_Interface
    target_VLR[6].attrib['Value'] = "::"
    target_VLR[7].attrib['Value'] = "0"
    target_VLR[8].attrib['Value'] = "lo"
    target_VLR[9].attrib['Value'] = "29118"
    target_VLR[10].attrib['Value'] = "SCTP"
    target_VLR[11].attrib['Value'] = "2"
    Start_IP+=1
    return Start_IP



def Test_EIR(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test EIR']
    test_EIR = Entity_Configuration[position_count[0]].getchildren()
    test_EIR[0].attrib['Value'] = "YES"
    test_EIR[1].attrib['Value'] = "S13"
    test_EIR[2].attrib['Value'] = "3868"
    test_EIR[3].attrib['Value'] = "NO"
    test_EIR[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_EIR[5].attrib['Value'] = IPv4_Mask
    test_EIR[6].attrib['Value'] = IPv4_Interface
    test_EIR[7].attrib['Value'] = "::"
    test_EIR[8].attrib['Value'] = "0"
    test_EIR[9].attrib['Value'] = "lo"
    test_EIR[10].attrib['Value'] = "SCTP"
    test_EIR[11].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_EIR[12].attrib['Value'] = "eir.epc.mnc001.mcc001.3gppnetwork.org"
    Start_IP+=1
    return Start_IP


def Test_DRA(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test DRA']
    test_DRA = Entity_Configuration[position_count[0]].getchildren()
    test_DRA[0].attrib['Value'] = "NO"
    test_DRA[1].attrib['Value'] = "S6a"
    test_DRA[2].attrib['Value'] = "3588"
    test_DRA[3].attrib['Value'] = "NO"
    test_DRA[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_DRA[5].attrib['Value'] = IPv4_Mask
    test_DRA[6].attrib['Value'] = IPv4_Interface
    test_DRA[7].attrib['Value'] = "::"
    test_DRA[8].attrib['Value'] = "0"
    test_DRA[9].attrib['Value'] = "lo"
    test_DRA[10].attrib['Value'] = "Proxy"
    test_DRA[11].attrib['Value'] = "SCTP"
    test_DRA[12].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_DRA[13].attrib['Value'] = "dra.epc.mnc001.mcc001.3gppnetwork.org"
    Start_IP+=1
    return Start_IP


def Test_GMLC(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test GMLC']
    test_GMLC = Entity_Configuration[position_count[0]].getchildren()
    test_GMLC[0].attrib['Value'] = "NO"
    test_GMLC[1].attrib['Value'] = "SLg"
    test_GMLC[2].attrib['Value'] = "3868"
    test_GMLC[3].attrib['Value'] = "NO"
    test_GMLC[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_GMLC[5].attrib['Value'] = IPv4_Mask
    test_GMLC[6].attrib['Value'] = IPv4_Interface
    test_GMLC[7].attrib['Value'] = "::"
    test_GMLC[8].attrib['Value'] = "0"
    test_GMLC[9].attrib['Value'] = "lo"
    test_GMLC[10].attrib['Value'] = "SCTP"
    test_GMLC[11].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_GMLC[12].attrib['Value'] = "polarisnetworks.epc.mnc001.mcc001.3gppnetwork.org"
    test_GMLC[13].attrib['Value'] = "920000001"
    Start_IP+=1
    return Start_IP


def Test_IWS(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test IWS']
    test_IWS = Entity_Configuration[position_count[0]].getchildren()
    test_IWS[0].attrib['Value'] = "NO"
    test_IWS[1].attrib['Value'] = "S102"
    test_IWS[2].attrib['Value'] = "NO"
    test_IWS[3].attrib['Value'] = Base_IP+str(Start_IP)
    test_IWS[4].attrib['Value'] = IPv4_Mask
    test_IWS[5].attrib['Value'] = IPv4_Interface
    test_IWS[6].attrib['Value'] = "::"
    test_IWS[7].attrib['Value'] = "0"
    test_IWS[8].attrib['Value'] = "lo"
    test_IWS[9].attrib['Value'] = "23272"
    test_IWS[10].attrib['Value'] = "3"
    test_IWS[11].attrib['Value'] = "UDP"
    test_IWS[12].attrib['Value'] = "Test IWS"
    Start_IP+=1
    return Start_IP


def Test_ESMLC(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test ESMLC']
    test_ESMLC = Entity_Configuration[position_count[0]].getchildren()
    test_ESMLC[0].attrib['Value'] = "NO"
    test_ESMLC[1].attrib['Value'] = "SLs"
    test_ESMLC[2].attrib['Value'] = "NO"
    test_ESMLC[3].attrib['Value'] = Base_IP+str(Start_IP)
    test_ESMLC[4].attrib['Value'] = IPv4_Mask
    test_ESMLC[5].attrib['Value'] = IPv4_Interface
    test_ESMLC[6].attrib['Value'] = "::"
    test_ESMLC[7].attrib['Value'] = "0"
    test_ESMLC[8].attrib['Value'] = "lo"
    test_ESMLC[9].attrib['Value'] = "SCTP"
    Start_IP+=1
    return Start_IP



def Test_DNS(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test DNS']
    test_DNS = Entity_Configuration[position_count[0]].getchildren()
    test_DNS[0].attrib['Value'] = "NO"
    test_DNS[1].attrib['Value'] = "UDP"
    test_DNS[2].attrib['Value'] = "NO"
    test_DNS[3].attrib['Value'] = "0.0.0.0"
    test_DNS[4].attrib['Value'] = "0"
    test_DNS[5].attrib['Value'] = "0.0.0.0"
    test_DNS[6].attrib['Value'] = "::"
    test_DNS[7].attrib['Value'] = "0"
    test_DNS[8].attrib['Value'] = "::"
    test_DNS[9].attrib['Value'] = "53"
    test_DNS[10].attrib['Value'] = "polarisnetworks.net"



def Test_IPSec(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'IPSec']
    test_IPSec = Entity_Configuration[position_count[0]].getchildren()
    test_IPSec[0].attrib['Value'] = "NO"
    test_IPSec[1].attrib['Value'] = "HSS-EIR"
    test_IPSec[2].attrib['Value'] = "123456789"
    test_IPSec[3].attrib['Value'] = "MD5"
    test_IPSec[4].attrib['Value'] = "DES"
    test_IPSec[5].attrib['Value'] = "Medium(2)"
    test_IPSec[6].attrib['Value'] = "Both"
    test_IPSec[7].attrib['Value'] = "sha"
    test_IPSec[8].attrib['Value'] = "3des"
    test_IPSec[9].attrib['Value'] = "sha"



def Logger(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Logger']
    logger = Entity_Configuration[position_count[0]].getchildren()
    logger[0].attrib['Value'] = "Last_Run"
    


def Timers(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Timers']
    timers = Entity_Configuration[position_count[0]].getchildren()
    timers[0].attrib['Value'] = "30000"
    timers[1].attrib['Value'] = "40000"




#Main :---------------

IPv4_Mask = "24"
IPv4_Interface = "ens33"
emulatorStartIp = sys.argv[1] # Emulator Start IP Address : '192.168.233.100'
emulatorStartIp = emulatorStartIp.split('.')
Base_IP = emulatorStartIp[0]+'.'+emulatorStartIp[1]+'.'+emulatorStartIp[2]+'.'
Start_IP = int(emulatorStartIp[3])

Entity_List = []
DUT_List = []



tree = ET.parse('/home/polaris/polarisft/mme/ConfigData.xml')
root_configuration = tree.getroot()
Entity_Configuration = root_configuration.getchildren()


#['Device Under Test','Home PLMN', 'Visited PLMN', 'Unknown PLMN', 'Test MME', 'Test UE', 'Unknown UE', 'Test eNodeB']
#['Handover Target eNodeB', 'Test CBC', 'Test SGW', 'Handover Target SGW', 'Test SGSN', 'Test VLR', 'Target VLR']
#['Test HSS', 'Test EIR', 'Test DRA', 'Test GMLC', 'Test DNS', 'IPSec', 'Test IWS', 'Test ESMLC', 'Logger', 'Timers']
[Entity_List.append(Entity_Configuration[elements].attrib['Name']) for elements in range(0,len(Entity_Configuration))]

#['General', 'Interfaces']
[DUT_List.append(Entity_Configuration[0][elements].attrib['Name']) for elements in range(0,len(Entity_Configuration[0]))]




#Configuration : General
General(Entity_Configuration,DUT_List)

#Configuration : Interfaces
Start_IP = Interfaces(Entity_Configuration,DUT_List, Start_IP)

#Configuration : Home_PLMN
Home_PLMN(Entity_Configuration,Entity_List)

#Configuration : Visited_PLMN
Visited_PLMN(Entity_Configuration,Entity_List)

#Configuration : Unknown_PLMN
Unknown_PLMN(Entity_Configuration,Entity_List)

#Configuration : Test_UE
Test_UE(Entity_Configuration, Entity_List)

#Configuration : Test_Unknown_UE
Test_Unknown_UE(Entity_Configuration, Entity_List)

#Configuration : Test_DNS
Test_DNS(Entity_Configuration, Entity_List)

#Configuration : Test_IPSec
Test_IPSec(Entity_Configuration, Entity_List)

#Configuration : Logger
Logger(Entity_Configuration, Entity_List)

#Configuration : Timers
Timers(Entity_Configuration, Entity_List)



    
#Configuration : Test HSS(1) and PGW(2)
Start_IP = Test_HSS(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test SGW(3)
Start_IP = Test_SGW(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Target SGW(4)
Start_IP = Target_SGW(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test SGSN(5)
Start_IP = Test_SGSN(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test VLR(6)
Start_IP = Test_VLR(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Target VLR(7)
Start_IP = Target_VLR(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test MME(8)
Start_IP = Test_MME(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test EIR(9)
Start_IP = Test_EIR(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test DRA(10)
Start_IP = Test_DRA(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test GMLC(11)
Start_IP = Test_GMLC(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test IWS(12)
Start_IP = Test_IWS(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test ESMLC(13)
Start_IP = Test_ESMLC(Entity_Configuration, Entity_List, Start_IP)




#Configuration : Test eNodeB(14)
Start_IP = Test_eNodeB(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Handover Target eNodeB(15)
Start_IP = Target_eNodeB(Entity_Configuration, Entity_List, Start_IP)

#Configuration : Test CBC(16)
Start_IP = Test_CBC(Entity_Configuration, Entity_List, Start_IP)


#Write the changes into Configuration File : ---
tree.write('/home/polaris/polarisft/mme/ConfigData.xml')   



