# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""


import Tkinter
import paramiko
import sys


commComm = 'comm::comm send [list '
hostName = sys.argv[1]        # Emulator IP Address : '192.168.233.133'
port = sys.argv[2]            # Emulator Port : 5676
userName = 'root'             # Emulator username : root
passWord = 'polaris'          # Emulator password : polaris
emulatorStartIp = sys.argv[3] # Emulator Start IP Address : '192.168.233.100'



#Format : python    FileName.py     emulator_management_IP     emulator_Port     emulator_start_IP

#----------- SSH Login to Emulator and starting up Nettest Server -------------
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostName, username=userName, password=passWord)

client.exec_command('cd /opt/polaris-nettest/server/; ./stop.sh')
stdin, stdout, stderr = client.exec_command('cd /opt/polaris-nettest/server/; ./start.sh -mme '+port)
for line in stdout:
    print (line.strip('\n'))
    
client.close()




#--------------------- Assigning IP Addresses to all Nodes ----------------------
emulatorStartIp = emulatorStartIp.split('.')
baseIp = emulatorStartIp[0]+'.'+emulatorStartIp[1]+'.'+emulatorStartIp[2]+'.'
startIp = int(emulatorStartIp[3])
mmeIp = baseIp+str(startIp)
startIp+=1
testHssIp = baseIp+str(startIp)
startIp+=1
testPgwIp = baseIp+str(startIp)
startIp+=1
testSgwIp = baseIp+str(startIp)
startIp+=1
targetSgwIp = baseIp+str(startIp)
startIp+=1
testSgsnIp = baseIp+str(startIp)
startIp+=1
testVlrIp = baseIp+str(startIp)
startIp+=1
targetVlrIp = baseIp+str(startIp)
startIp+=1
testMmeIp = baseIp+str(startIp)
startIp+=1
testEir = baseIp+str(startIp)
startIp+=1
testDra = baseIp+str(startIp)
startIp+=1
testGmlc = baseIp+str(startIp)
startIp+=1
testIws = baseIp+str(startIp)
startIp+=1
testEsmlc = baseIp+str(startIp)
startIp+=1



#------------------API Calls to Emulator Server for Configuration--------------------
tkinterobj=Tkinter.Tk()
tkinterobj.tk.eval('source comm-package.tcl')

command = ['PNReset',
            '{PNConnect -statusUpdateHook :emulator:1 -statusUpdatePort 50476 -reset 0}',
            '{EmuConfigureMaxMonitoredSubscriber -maxSubscriber 10}',
            'EmuMMEGetSubscriberStatus','{EmuConfigureSubscriberMonitoring -status ENABLE}',
            '{PNConfigureEventLogging -level Information -compression 0 -enableDevLog 0 -repetition 1 -size 20 -maxFileSize 10 -compressedFileCount 50}',
            '{PNCreateNetwork -plmn TestNetwork -mcc 001 -mnc 01}',
            '{PNMMEConfigureEmergencySession -enable 1  -emrSvcType 2  -apn emergency-apn.TestNetwork  -pgwIPAddress '+testPgwIp+'  -apnAMBRUL 4096  -apnAMBRDL 4096  -qci 6  -pvi 0  -pl 6  -pci 0  -plmn TestNetwork -validateIMEI 1 -s5Protocol 2}',
            '{PNMMEConfigureCSFallback -plmn TestNetwork -enable 1  -csNetwork 3 -serviceType 2}',
            '{PNMMEConfigureIWS -plmn TestNetwork  -ipAddress '+testIws+' -port 23272 -operationType 0}',       
            '{PNMMEConfigurePoolArea -plmn TestNetwork -poolAreaID 32768 -tacList "1,2"}',
            '{PNConfigureTrackingArea -plmn TestNetwork  -hourOffset 8  -minuteOffset 00  -offsetSign 1 -dst 0  -tacList "All" -timeZoneIndex 4 -allowHGW 1}']

for i in range(0,len(command)):
  tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+command[i])
 
mmeHandle = tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMECreate  -groupID "32768"  -code "1"  -plmn TestNetwork  -group 0 -intfS1MME ens33 -ipaddrS1MME '+mmeIp+' -netmaskS1MME 24 -portS1MME 36412 -intfS11 ens33 -ipaddrS11 '+mmeIp+' -netmaskS11 24 -portS11 2123 -intfS11u ens33 -ipaddrS11u '+mmeIp+' -netmaskS11u 24 -portS11u 2152 -intfS10 ens33 -ipaddrS10 '+mmeIp+' -netmaskS10 24 -portS10 2123 -intfS6a ens33 -ipaddrS6a '+mmeIp+' -netmaskS6a 24 -portS6a 3868 -intfS13 ens33 -ipaddrS13 '+mmeIp+' -netmaskS13 24 -portS13 3868 -intfS3 ens33 -ipaddrS3 '+mmeIp+' -netmaskS3 24 -portS3 2123 -intfSBc ens33 -ipaddrSBc '+mmeIp+' -netmaskSBc 24 -portSBc 29168 -intfSGs ens33 -ipaddrSGs '+mmeIp+' -netmaskSGs 24 -portSGs 29118 -intfS102 ens33 -ipaddrS102 '+mmeIp+' -netmaskS102 24 -portS102 23272 -intfM3 ens33 -ipaddrM3 '+mmeIp+' -netmaskM3 24 -portM3 36444 -intfSm ens33 -ipaddrSm '+mmeIp+' -netmaskSm 24 -portSm 2123 -intfSLg ens33 -ipaddrSLg '+mmeIp+' -netmaskSLg 24 -portSLg 3868 -intfSLs ens33 -ipaddrSLs '+mmeIp+' -netmaskSLs 24 -portSLs 9082 -intfSv ens33 -ipaddrSv '+mmeIp+' -netmaskSv 24 -portSv 2123 -name "mmec01.mmegi8000.mme" -diaRealm "epc.mnc001.mcc001.3gppnetwork.org" -partialPathFailure 1 -nodeIdType 0 -nodeIdValue '+mmeIp+' -pgwRestart 1 -modifyAccessBearer 1 -serviceRestoration 1 -diaProxyPA2 0 -imsSupportIndication 1 -ueRadioCapMatch 0 -preferIPv4Address 1}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureDiameterAVPValidation -mme '+str(mmeHandle)+' -isFailedAvpEnabled 0}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureNbrSGW -mme '+str(mmeHandle)+' -ipAddress '+testSgwIp+' -plmn TestNetwork -tacList "1"  -s5s8Protocol 2}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureNbrSGW -mme '+str(mmeHandle)+' -ipAddress '+targetSgwIp+' -plmn TestNetwork -tacList "2"  -s5s8Protocol 2}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureNbrPGW -mme '+str(mmeHandle)+'  -ipAddress '+testPgwIp+'  -plmn TestNetwork  -s5s8Protocol 2 -apnList *}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureNbrHSS -mme '+str(mmeHandle)+'  -plmn TestNetwork  -ipAddress '+testHssIp+'  -port 3868  -type 0 -realm epc.mnc001.mcc001.3gppnetwork.org}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureNbrMME -mme '+str(mmeHandle)+' -plmn TestNetwork -groupID 32768  -code 2 -ipAddress '+testMmeIp+'  -tacList "2"}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureNbrSGSN -mme '+str(mmeHandle)+'  -ipAddress '+testSgsnIp+'  -plmn TestNetwork  -lac 1  -rac 1}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEConfigureCapabilities -mme '+str(mmeHandle)+' -relayNode 1 -ueStoreAMBR 0 -disableEMMInfo 0 -enableMacroENBBehindHGW 0 -allowUnknownAPNOverride 1 -imsSupportIndication 1}')

tkinterobj.tk.eval(commComm+str(port)+' '+hostName+'] '+'{PNMMEUpdateConfiguration -mme '+str(mmeHandle)+' -capacity 1}')

