# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 10:50:54 2017

@author: dipak
"""

from xml.etree import ElementTree as ET
import sys




def General(Entity_Configuration, DUT_List, entity_position):
    
    position_count = [position_count for position_count,position in enumerate(DUT_List) if position == 'General']
    test_General = Entity_Configuration[entity_position].getchildren()[position_count[0]]
    test_General[0].attrib['Value'] = "pgw1.ubuntu.epc.mnc001.mcc001.3gppnetwork.org"
    test_General[1].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_General[2].attrib['Value'] = "YES"
    test_General[3].attrib['Value'] = "NO"
    test_General[4].attrib['Value'] = "DRA.epc.mnc001.mcc001.3gppnetwork.org"
    test_General[5].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_General[6].attrib['Value'] = "6"
    test_General[7].attrib['Value'] = "Release_11"
    test_General[8].attrib['Value'] = "Static PCC Rule"
    test_General[9].attrib['Value'] = "NO"

    
    

def Interfaces(Entity_Configuration, DUT_List, Start_IP, entity_position):
    
    position_count = [position_count for position_count,position in enumerate(DUT_List) if position == 'Interfaces']
    IPv4 = (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[0]
    IPv6 = (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[1]
    
    #Configuration : IPv4
    IPv4[0].attrib['Value'] = "SPGW"
    IPv4[1].attrib['Value'] = "S4S11 IP Address"
    IPv4[2].attrib['Value'] = Base_IP+str(Start_IP)
    IPv4[4].attrib['Value'] = Base_IP+str(Start_IP)
    Start_IP+=1
    IPv4[3].attrib['Value'] = Base_IP+str(Start_IP)
    
    ipv4_ip_count = 5
    while ipv4_ip_count!=10:
        #Start_IP+=1
        IPv4[ipv4_ip_count].attrib['Value'] = Base_IP+str(Start_IP)
        ipv4_ip_count +=1
    
    '''
    #Configuration : IPv6
    ipv6_ip_count = 0
    while ipv6_ip_count!=12:
        #Start_IP+=1
        IPv6[ipv6_ip_count].attrib['Value'] = Base_IP+str(Start_IP)
        ipv6_ip_count +=1
    ''' 
    Start_IP+=1
    return Start_IP



def Home_PLMN(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Home PLMN']
    home_PLMN = Entity_Configuration[position_count[0]].getchildren()
    home_PLMN[0].attrib['Value'] = "001"
    home_PLMN[1].attrib['Value'] = "01"




def Visited_PLMN(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Visited PLMN']
    visited_PLMN = Entity_Configuration[position_count[0]].getchildren()
    visited_PLMN[0].attrib['Value'] = "403"
    visited_PLMN[1].attrib['Value'] = "31"
    


def Default_PDN(Entity_Configuration, default_PDN_List, entity_position):
    
    position_count = [position_count for position_count,position in enumerate(default_PDN_List) if position == 'General']
    general =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    general[0].attrib['Value'] = "apn.TestNetwork.mnc001.mcc001.gprs"
    general[1].attrib['Value'] = "15.15.15.15"
    
    position_count = [position_count for position_count,position in enumerate(default_PDN_List) if position == 'QoS']
    qos =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    qos[0].attrib['Value'] = "9"
    qos[1].attrib['Value'] = "5"
    qos[2].attrib['Value'] = "Enabled(0)"
    qos[3].attrib['Value'] = "Disabled(1)"
    qos[4].attrib['Value'] = "3001"
    qos[5].attrib['Value'] = "3001"
    
    position_count = [position_count for position_count,position in enumerate(default_PDN_List) if position == 'TFT']
    tft =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    tft[0].attrib['Value'] = "5"
    tft[1].attrib['Value'] = "permit out ip from any to any"
    
    position_count = [position_count for position_count,position in enumerate(default_PDN_List) if position == 'Charging']
    charging =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    charging[0].attrib['Value'] = "Default PCC Rule"
    charging[1].attrib['Value'] = "1"
    charging[2].attrib['Value'] = "2"
    charging[3].attrib['Value'] = "2"
    charging[4].attrib['Value'] = "DISABLE_ONLINE(0)"
    charging[5].attrib['Value'] = "DISABLE_OFFLINE(0)"
    charging[6].attrib['Value'] = "2"
    charging[7].attrib['Value'] = "0"
    charging[8].attrib['Value'] = "20"
    charging[9].attrib['Value'] = "20060"
    
    

def Secondary_PDN(Entity_Configuration, secondary_PDN_List, entity_position):
    
    position_count = [position_count for position_count,position in enumerate(secondary_PDN_List) if position == 'General']
    general =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    general[0].attrib['Value'] = "apn1.TestNetwork.mnc001.mcc001.gprs"
    general[1].attrib['Value'] = "50.50.50.50"
    
    position_count = [position_count for position_count,position in enumerate(secondary_PDN_List) if position == 'QoS']
    qos =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    qos[0].attrib['Value'] = "8"
    qos[1].attrib['Value'] = "2"
    qos[2].attrib['Value'] = "Enabled(0)"
    qos[3].attrib['Value'] = "Disabled(1)"
    qos[4].attrib['Value'] = "4000"
    qos[5].attrib['Value'] = "4000"
    
    position_count = [position_count for position_count,position in enumerate(secondary_PDN_List) if position == 'TFT']
    tft =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    tft[0].attrib['Value'] = "2"
    tft[1].attrib['Value'] = "permit out ip from any to any"
    
    position_count = [position_count for position_count,position in enumerate(secondary_PDN_List) if position == 'Charging']
    charging =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    charging[0].attrib['Value'] = "Default PCC Rule 2"
    charging[1].attrib['Value'] = "7"
    charging[2].attrib['Value'] = "2"
    charging[3].attrib['Value'] = "2"
    charging[4].attrib['Value'] = "DISABLE_ONLINE(0)"
    charging[5].attrib['Value'] = "DISABLE_OFFLINE(0)"
    charging[6].attrib['Value'] = "DURATION(0)"
    charging[7].attrib['Value'] = "0"
    charging[8].attrib['Value'] = "60"
    charging[9].attrib['Value'] = "0"
    
    


def Emergency_PDN(Entity_Configuration, emergency_PDN_List, entity_position):
    
    position_count = [position_count for position_count,position in enumerate(emergency_PDN_List) if position == 'General']
    general =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    general[0].attrib['Value'] = "emergengy-apn.TestNetwork.mnc001.mcc001.gprs"
    
    position_count = [position_count for position_count,position in enumerate(emergency_PDN_List) if position == 'QoS']
    qos =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    qos[0].attrib['Value'] = "7"
    qos[1].attrib['Value'] = "2"
    qos[2].attrib['Value'] = "Enabled(0)"
    qos[3].attrib['Value'] = "Disabled(1)"
    qos[4].attrib['Value'] = "3001"
    qos[5].attrib['Value'] = "3001"
    
    position_count = [position_count for position_count,position in enumerate(emergency_PDN_List) if position == 'TFT']
    tft =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    tft[0].attrib['Value'] = "2"
    tft[1].attrib['Value'] = "permit out ip from any to any"
    
    position_count = [position_count for position_count,position in enumerate(emergency_PDN_List) if position == 'Charging']
    charging =  (Entity_Configuration[entity_position].getchildren()[position_count[0]])
    charging[0].attrib['Value'] = "Emergency PCC Rule"
    charging[1].attrib['Value'] = "7"
    charging[2].attrib['Value'] = "2"
    charging[3].attrib['Value'] = "2"
    charging[4].attrib['Value'] = "ENABLE_ONLINE(1)"
    charging[5].attrib['Value'] = "DISABLE_OFFLINE(0)"
    charging[6].attrib['Value'] = "2"
    charging[7].attrib['Value'] = "0"
    charging[8].attrib['Value'] = "60"
    charging[9].attrib['Value'] = "0"
    
    


def Test_PCRF(Entity_Configuration, test_PCRF_List, Start_IP, entity_position):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test PCRF']
    test_PCRF = Entity_Configuration[position_count[0]].getchildren()
    test_PCRF[0].attrib['Value'] = "YES"
    test_PCRF[1].attrib['Value'] = "Gx"
    test_PCRF[2].attrib['Value'] = "YES"
    test_PCRF[3].attrib['Value'] = "pcrf.epc.mnc001.mcc001.3gppnetwork.org"
    test_PCRF[4].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_PCRF[5].attrib['Value'] = Base_IP+str(Start_IP)
    test_PCRF[6].attrib['Value'] = IPv4_Mask
    test_PCRF[7].attrib['Value'] = IPv4_Interface
    test_PCRF[8].attrib['Value'] = "::"
    test_PCRF[9].attrib['Value'] = "0"
    test_PCRF[10].attrib['Value'] = "lo"
    test_PCRF[11].attrib['Value'] = "NO"
    test_PCRF[12].attrib['Value'] = "TCP"
    test_PCRF[13].attrib['Value'] = "3868"

    position_count = [position_count for position_count,position in enumerate(test_PCRF_List) if position == 'Dedicated Bearer Rule']
    charging =  (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[0]#Charging
    charging[0].attrib['Value'] = "Dedicated PCC Rule"
    charging[1].attrib['Value'] = "4131"
    charging[2].attrib['Value'] = "2"
    charging[3].attrib['Value'] = "2"
    charging[4].attrib['Value'] = "ENABLE_ONLINE(1)"
    charging[5].attrib['Value'] = "DISABLE_OFFLINE(0)"
    charging[6].attrib['Value'] = "2"
    charging[7].attrib['Value'] = "0"
    charging[8].attrib['Value'] = "30"
    charging[9].attrib['Value'] = "0"
    
    
    qos =  (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[1]#QoS
    qos[0].attrib['Value'] = "3"
    qos[1].attrib['Value'] = "6"
    qos[2].attrib['Value'] = "Enabled(0)"
    qos[3].attrib['Value'] = "Disabled(1)"
    qos[4].attrib['Value'] = "RATING_GROUP_LEVEL(1)"
    qos[5].attrib['Value'] = "2000"
    qos[6].attrib['Value'] = "2000"
    qos[7].attrib['Value'] = "1000"
    qos[8].attrib['Value'] = "1000"
    qos[9].attrib['Value'] = "2000"
    qos[10].attrib['Value'] = "2000"
    
    tft =  (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[2]#TFT
    tft[0].attrib['Value'] = "2"
    tft[1].attrib['Value'] = "permit out ip from any to any"
    
    
    
    position_count = [position_count for position_count,position in enumerate(test_PCRF_List) if position == 'Modified Bearer Rule']
    charging =  (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[0]#Charging
    charging[0].attrib['Value'] = "8"
    charging[1].attrib['Value'] = "3"
    charging[2].attrib['Value'] = "3"
    charging[3].attrib['Value'] = "ENABLE_ONLINE(1)"
    charging[4].attrib['Value'] = "DISABLE_OFFLINE(0)"
    charging[5].attrib['Value'] = "2"
    charging[6].attrib['Value'] = "0"
    charging[7].attrib['Value'] = "60"
    charging[8].attrib['Value'] = "0"
    
    
    qos =  (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[1]#QoS
    qos[0].attrib['Value'] = "8"
    qos[1].attrib['Value'] = "7"
    qos[2].attrib['Value'] = "Enabled(0)"
    qos[3].attrib['Value'] = "Disabled(1)"
    qos[4].attrib['Value'] = "RATING_GROUP_LEVEL(1)"
    qos[5].attrib['Value'] = "4000"
    qos[6].attrib['Value'] = "4000"
    qos[7].attrib['Value'] = "3000"
    qos[8].attrib['Value'] = "3000"
    qos[9].attrib['Value'] = "3000"
    qos[10].attrib['Value'] = "3000"
    
    tft =  (Entity_Configuration[entity_position].getchildren()[position_count[0]]).getchildren()[2]#TFT
    tft[0].attrib['Value'] = "2"
    tft[1].attrib['Value'] = "permit out ip from any to any"
    
    
    Start_IP+=1
    return Start_IP



def Test_OCS(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test OCS']
    test_OCS = Entity_Configuration[position_count[0]].getchildren()
    test_OCS[0].attrib['Value'] = "NO"
    test_OCS[1].attrib['Value'] = "Gy"
    test_OCS[2].attrib['Value'] = "ocs.epc.mnc001.mcc001.3gppnetwork.org"
    test_OCS[3].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_OCS[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_OCS[5].attrib['Value'] = IPv4_Mask
    test_OCS[6].attrib['Value'] = IPv4_Interface
    test_OCS[7].attrib['Value'] = "::"
    test_OCS[8].attrib['Value'] = "0"
    test_OCS[9].attrib['Value'] = "::"
    test_OCS[10].attrib['Value'] = "NO"
    test_OCS[11].attrib['Value'] = "SCTP"
    test_OCS[12].attrib['Value'] = "30"
    test_OCS[13].attrib['Value'] = "1"
    Start_IP+=1
    return Start_IP
    



def Test_OFCS(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test OFCS']
    test_OFCS = Entity_Configuration[position_count[0]].getchildren()
    test_OFCS[0].attrib['Value'] = "NO"
    test_OFCS[1].attrib['Value'] = "Gz"
    test_OFCS[2].attrib['Value'] = "ofcs.epc.mnc001.mcc001.3gppnetwork.org"
    test_OFCS[3].attrib['Value'] = "epc.mnc001.mcc001.3gppnetwork.org"
    test_OFCS[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_OFCS[5].attrib['Value'] = IPv4_Mask
    test_OFCS[6].attrib['Value'] = IPv4_Interface
    test_OFCS[7].attrib['Value'] = "::"
    test_OFCS[8].attrib['Value'] = "0"
    test_OFCS[9].attrib['Value'] = "::"
    test_OFCS[10].attrib['Value'] = "NO"
    test_OFCS[11].attrib['Value'] = "SCTP"
    test_OFCS[12].attrib['Value'] = "50"
    Start_IP+=1
    return Start_IP



def Test_UE(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test UE']
    test_UE = Entity_Configuration[position_count[0]].getchildren()
    test_UE[0].attrib['Value'] = "9990000001"
    test_UE[1].attrib['Value'] = "919293949596979"
    test_UE[2].attrib['Value'] = "32"
    test_UE[3].attrib['Value'] = "0"
    test_UE[4].attrib['Value'] = "Vodafone"
    test_UE[5].attrib['Value'] = "700"
    test_UE[6].attrib['Value'] = "700"
    test_UE[7].attrib['Value'] = "500"
    test_UE[8].attrib['Value'] = "500"
   



def Test_eNodeB(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test eNodeB']
    test_eNodeB = Entity_Configuration[position_count[0]].getchildren()
    test_eNodeB[0].attrib['Value'] = "S1/S4/S12U"
    test_eNodeB[1].attrib['Value'] = "YES"
    test_eNodeB[2].attrib['Value'] = "Ethernet"
    test_eNodeB[3].attrib['Value'] = "NO"
    test_eNodeB[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_eNodeB[5].attrib['Value'] = IPv4_Mask
    test_eNodeB[6].attrib['Value'] = IPv4_Interface
    test_eNodeB[7].attrib['Value'] = "::"
    test_eNodeB[8].attrib['Value'] = "0"
    test_eNodeB[9].attrib['Value'] = "lo"
    test_eNodeB[10].attrib['Value'] = "2152"
    Start_IP+=1
    return Start_IP

   

def Target_eNodeB(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Handover Target eNodeB']
    target_eNodeB = Entity_Configuration[position_count[0]].getchildren()
    target_eNodeB[0].attrib['Value'] = "YES"
    target_eNodeB[1].attrib['Value'] = "S4/S11"
    target_eNodeB[2].attrib['Value'] = "Ethernet"
    target_eNodeB[3].attrib['Value'] = "NO"
    target_eNodeB[4].attrib['Value'] = Base_IP+str(Start_IP)
    target_eNodeB[5].attrib['Value'] = IPv4_Mask
    target_eNodeB[6].attrib['Value'] = IPv4_Interface
    target_eNodeB[7].attrib['Value'] = "::"
    target_eNodeB[8].attrib['Value'] = "0"
    target_eNodeB[9].attrib['Value'] = "lo"
    target_eNodeB[10].attrib['Value'] = "2152"
    Start_IP+=1
    return Start_IP




def Test_MME(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test MME']
    test_MME = Entity_Configuration[position_count[0]].getchildren()
    test_MME[0].attrib['Value'] = "S4/S11"
    test_MME[1].attrib['Value'] = "Ethernet"
    test_MME[2].attrib['Value'] = "NO"
    test_MME[3].attrib['Value'] = Base_IP+str(Start_IP)
    test_MME[4].attrib['Value'] = IPv4_Mask
    test_MME[5].attrib['Value'] = IPv4_Interface
    test_MME[6].attrib['Value'] = "::"
    test_MME[7].attrib['Value'] = "0"
    test_MME[8].attrib['Value'] = "lo"
    test_MME[9].attrib['Value'] = "2123"
    test_MME[10].attrib['Value'] = "Test MME"
    test_MME[11].attrib['Value'] = "99"
    test_MME[12].attrib['Value'] = "32780"
    Start_IP+=1
    return Start_IP



def Target_MME(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Handover Target MME']
    target_MME = Entity_Configuration[position_count[0]].getchildren()
    target_MME[0].attrib['Value'] = "YES"
    target_MME[1].attrib['Value'] = "S4/S11"
    target_MME[2].attrib['Value'] = "Ethernet"
    target_MME[3].attrib['Value'] = "NO"
    target_MME[4].attrib['Value'] = Base_IP+str(Start_IP)
    target_MME[5].attrib['Value'] = IPv4_Mask
    target_MME[6].attrib['Value'] = IPv4_Interface
    target_MME[7].attrib['Value'] = "::"
    target_MME[8].attrib['Value'] = "0"
    target_MME[9].attrib['Value'] = "lo"
    target_MME[10].attrib['Value'] = "Target MME"
    target_MME[11].attrib['Value'] = "98"
    target_MME[12].attrib['Value'] = "32785"
    Start_IP+=1
    return Start_IP





def Test_SGSN(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test SGSN']
    test_SGSN = Entity_Configuration[position_count[0]].getchildren()
    test_SGSN[0].attrib['Value'] = "YES"
    test_SGSN[1].attrib['Value'] = "S4/S11"
    test_SGSN[2].attrib['Value'] = "Ethernet"
    test_SGSN[3].attrib['Value'] = "NO"
    test_SGSN[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_SGSN[5].attrib['Value'] = IPv4_Mask
    test_SGSN[6].attrib['Value'] = IPv4_Interface
    test_SGSN[7].attrib['Value'] = "::"
    test_SGSN[8].attrib['Value'] = "0"
    test_SGSN[9].attrib['Value'] = "lo"
    test_SGSN[10].attrib['Value'] = "50"
    test_SGSN[11].attrib['Value'] = "1"
    test_SGSN[12].attrib['Value'] = "100"
    test_SGSN[13].attrib['Value'] = "151"
    test_SGSN[14].attrib['Value'] = "21"
    Start_IP+=1
    return Start_IP



def Test_SGW(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test SGW']
    test_SGW = Entity_Configuration[position_count[0]].getchildren()
    test_SGW[0].attrib['Value'] = "YES"
    test_SGW[1].attrib['Value'] = "S5/S8-c"
    test_SGW[2].attrib['Value'] = "NO"
    test_SGW[3].attrib['Value'] = Base_IP+str(Start_IP)
    test_SGW[4].attrib['Value'] = IPv4_Mask
    test_SGW[5].attrib['Value'] = IPv4_Interface
    test_SGW[6].attrib['Value'] = "::"
    test_SGW[7].attrib['Value'] = "0"
    test_SGW[8].attrib['Value'] = "lo"
    test_SGW[9].attrib['Value'] = "2123"
    test_SGW[10].attrib['Value'] = "2152"
    test_SGW[11].attrib['Value'] = "Ethernet"
    test_SGW[12].attrib['Value'] = "10"
    test_SGW[13].attrib['Value'] = "GTPC_GTPv1_U_PMIP_GRE_ICMP"
    Start_IP+=1
    return Start_IP



def Internet_Host_1(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Internet Host 1']
    internet_Host_1 = Entity_Configuration[position_count[0]].getchildren()
    internet_Host_1[0].attrib['Value'] = "YES"
    internet_Host_1[1].attrib['Value'] = "SGi"
    internet_Host_1[2].attrib['Value'] = "NO"
    internet_Host_1[3].attrib['Value'] = Base_IP+str(Start_IP)
    internet_Host_1[4].attrib['Value'] = IPv4_Mask
    internet_Host_1[5].attrib['Value'] = IPv4_Interface
    internet_Host_1[6].attrib['Value'] = "9874"
    Start_IP+=1
    return Start_IP
    


def Internet_Host_2(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Internet Host 2']
    internet_Host_2 = Entity_Configuration[position_count[0]].getchildren()
    internet_Host_2[0].attrib['Value'] = "YES"
    internet_Host_2[1].attrib['Value'] = "SGi"
    internet_Host_2[2].attrib['Value'] = "NO"
    internet_Host_2[3].attrib['Value'] = Base_IP+str(Start_IP)
    internet_Host_2[4].attrib['Value'] = IPv4_Mask
    internet_Host_2[5].attrib['Value'] = IPv4_Interface
    internet_Host_2[6].attrib['Value'] = "9875"
    Start_IP+=1
    return Start_IP



def Test_DHCP_Client(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test DHCP Client']
    test_DHCP_Client = Entity_Configuration[position_count[0]].getchildren()
    test_DHCP_Client[0].attrib['Value'] = "NO"
    test_DHCP_Client[1].attrib['Value'] = "68"
    test_DHCP_Client[2].attrib['Value'] = "000002030421"
    test_DHCP_Client[3].attrib['Value'] = "10"
    test_DHCP_Client[4].attrib['Value'] = Base_IP+str(Start_IP)
    test_DHCP_Client[5].attrib['Value'] = "576"
    test_DHCP_Client[6].attrib['Value'] = "YES"
    test_DHCP_Client[7].attrib['Value'] = "YES"
    test_DHCP_Client[8].attrib['Value'] = "YES"
    test_DHCP_Client[9].attrib['Value'] = "NO"
    test_DHCP_Client[10].attrib['Value'] = "YES"
    test_DHCP_Client[11].attrib['Value'] = "YES"
    test_DHCP_Client[12].attrib['Value'] = "YES"
    Start_IP+=1
    return Start_IP



def Test_DHCPv4_Server(Entity_Configuration, Entity_List, Start_IP):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test DHCPv4 Server']
    test_DHCPv4_Server = Entity_Configuration[position_count[0]].getchildren()
    test_DHCPv4_Server[0].attrib['Value'] = "YES"
    test_DHCPv4_Server[1].attrib['Value'] = "SGi"
    test_DHCPv4_Server[2].attrib['Value'] = "NO"
    test_DHCPv4_Server[3].attrib['Value'] = Base_IP+str(Start_IP)
    test_DHCPv4_Server[4].attrib['Value'] = "67"
    test_DHCPv4_Server[5].attrib['Value'] = "0"
    test_DHCPv4_Server[6].attrib['Value'] = "UDP"
    test_DHCPv4_Server[7].attrib['Value'] = "100.100.100.1"
    test_DHCPv4_Server[8].attrib['Value'] = "8.8.8.8"
    test_DHCPv4_Server[9].attrib['Value'] = "server.dhcpd.net"
    test_DHCPv4_Server[10].attrib['Value'] = "100.100.100.118"
    test_DHCPv4_Server[11].attrib['Value'] = "10"
    Start_IP+=1
    return Start_IP



def Test_DHCPv6_Server(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test DHCPv6 Server']
    test_DHCPv6_Server = Entity_Configuration[position_count[0]].getchildren()
    test_DHCPv6_Server[0].attrib['Value'] = "NO"
    test_DHCPv6_Server[1].attrib['Value'] = "YES"
    test_DHCPv6_Server[2].attrib['Value'] = "SGi"
    test_DHCPv6_Server[3].attrib['Value'] = "::"
    test_DHCPv6_Server[4].attrib['Value'] = "00:50:56:C0:00:08"
    test_DHCPv6_Server[5].attrib['Value'] = "150"
    test_DHCPv6_Server[6].attrib['Value'] = "300"
    test_DHCPv6_Server[7].attrib['Value'] = "300"
    test_DHCPv6_Server[8].attrib['Value'] = "400"



def Test_iPerf(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'iPerf']
    test_iPerf = Entity_Configuration[position_count[0]].getchildren()
    test_iPerf[0].attrib['Value'] = "YES"
    test_iPerf[1].attrib['Value'] = "0.0.0.0"
    test_iPerf[2].attrib['Value'] = "0.0.0.0"
    test_iPerf[3].attrib['Value'] = "9872"
    test_iPerf[4].attrib['Value'] = "1"
    test_iPerf[5].attrib['Value'] = "2"
    test_iPerf[6].attrib['Value'] = "100"
    test_iPerf[7].attrib['Value'] = "10"
    test_iPerf[8].attrib['Value'] = "Bits"


'''
def Logger(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Logger']
    logger = Entity_Configuration[position_count[0]].getchildren()
    logger[0].attrib['Value'] = "NO"
'''


def Timers(Entity_Configuration, Entity_List):
    
    position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Timers']
    timers = Entity_Configuration[position_count[0]].getchildren()
    timers[0].attrib['Value'] = "60000"
    timers[1].attrib['Value'] = "2000"
    



#Main :---------------

IPv4_Mask = "24"
IPv4_Interface = "ens33"
emulatorStartIp = sys.argv[1] # Emulator Start IP Address : '192.168.233.100'
emulatorStartIp = emulatorStartIp.split('.')
Base_IP = emulatorStartIp[0]+'.'+emulatorStartIp[1]+'.'+emulatorStartIp[2]+'.'
Start_IP = int(emulatorStartIp[3])

Entity_List = []
DUT_List = []
default_PDN_List = []
secondary_PDN_List = []
emergency_PDN_List = []
test_PCRF_List = []




tree = ET.parse('/home/polaris/polarisft/spgw/ConfigData.xml')
root_configuration = tree.getroot()
Entity_Configuration = root_configuration.getchildren()


#['Device Under Test','Home PLMN', 'Visited PLMN', 'Default PDN', 'Secondary PDN', 'Emergency PDN', 'Test UE', 'Test eNodeB']
#['Handover Target eNodeB', 'Test MME', 'Handover Target MME', 'Test SGSN', 'Test SGW', 'Test PCRF', 'Test OCS']
#['Test OFCS', 'Internet Host 1', 'Internet Host 2', 'Test DHCP Client', 'Test DHCPv4 Server', 'Test DHCPv6 Server', 'iPerf', 'Logger', 'Timers']
[Entity_List.append(Entity_Configuration[elements].attrib['Name']) for elements in range(0,len(Entity_Configuration))]

#['General', 'Interfaces']
position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Device Under Test']
[DUT_List.append(Entity_Configuration[position_count[0]][elements].attrib['Name']) for elements in range(0,len(Entity_Configuration[position_count[0]]))]

#Configuration : General
General(Entity_Configuration, DUT_List, position_count[0])

#Configuration : Interfaces(1,2)
Start_IP = Interfaces(Entity_Configuration,DUT_List, Start_IP, position_count[0])



#Configuration : Home_PLMN
Home_PLMN(Entity_Configuration,Entity_List)

#Configuration : Visited_PLMN
Visited_PLMN(Entity_Configuration,Entity_List)



#Configuration : Default_PDN
position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Default PDN']
[default_PDN_List.append(Entity_Configuration[position_count[0]][elements].attrib['Name']) for elements in range(0,len(Entity_Configuration[position_count[0]]))]

Default_PDN(Entity_Configuration,default_PDN_List,position_count[0])


#Configuration : Secondary_PDN
position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Secondary PDN']
[secondary_PDN_List.append(Entity_Configuration[position_count[0]][elements].attrib['Name']) for elements in range(0,len(Entity_Configuration[position_count[0]]))]

Secondary_PDN(Entity_Configuration, secondary_PDN_List, position_count[0])


#Configuration : Emergency_PDN
position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Emergency PDN']
[emergency_PDN_List.append(Entity_Configuration[position_count[0]][elements].attrib['Name']) for elements in range(0,len(Entity_Configuration[position_count[0]]))]

Emergency_PDN(Entity_Configuration, emergency_PDN_List, position_count[0])


#Configuration : Test_UE
Test_UE(Entity_Configuration, Entity_List)




#Configuration : Test_PCRF(3)
position_count = [position_count for position_count,position in enumerate(Entity_List) if position == 'Test PCRF']
[test_PCRF_List.append(Entity_Configuration[position_count[0]][elements].attrib['Name']) for elements in range(0,len(Entity_Configuration[position_count[0]]))]

Start_IP = Test_PCRF(Entity_Configuration, test_PCRF_List, Start_IP, position_count[0])



#Configuration : Test MME(4)
Start_IP = Test_MME(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Target MME(5)
Start_IP = Target_MME(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test SGW(6)
Start_IP = Test_SGW(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test SGSN(7)
Start_IP = Test_SGSN(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test_OCS(8)
Start_IP = Test_OCS(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test_OFCS(9)
Start_IP = Test_OFCS(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test eNodeB(10)
Start_IP = Test_eNodeB(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Handover Target eNodeB(11)
Start_IP = Target_eNodeB(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Internet Host 1 (12)
Start_IP = Internet_Host_1(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Internet Host 2 (13)
Start_IP = Internet_Host_2(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test DHCP Client (14)
Start_IP = Test_DHCP_Client(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test_DHCPv4_Server (15)
Start_IP = Test_DHCPv4_Server(Entity_Configuration, Entity_List, Start_IP)


#Configuration : Test_DHCPv6_Server
Test_DHCPv6_Server(Entity_Configuration, Entity_List)


#Configuration : Test_iPerf
Test_iPerf(Entity_Configuration, Entity_List)


#Configuration : Logger
#Logger(Entity_Configuration, Entity_List)


#Configuration : Timers
Timers(Entity_Configuration, Entity_List)



tree.write('/home/polaris/polarisft/spgw/ConfigData.xml')