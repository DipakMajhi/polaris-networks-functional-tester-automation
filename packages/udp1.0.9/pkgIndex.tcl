global tcl_platform
if { [string match "Linux" $tcl_platform(os)] && [string match "x86_64" $tcl_platform(machine)] } {
        package ifneeded udp 1.0.9 \
                [list load [file join $dir libudp1.0.9.so]]
}
