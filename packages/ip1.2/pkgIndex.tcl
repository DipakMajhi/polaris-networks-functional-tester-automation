
# @@ Meta Begin
# Package ip 1.2
# Meta activestatetags ActiveTcl Public Tcllib
# Meta as::build::date 2011-07-14
# Meta as::origin      http://sourceforge.net/projects/tcllib
# Meta category        Domain Name Service
# Meta description     IPv4 and IPv6 address manipulation
# Meta license         BSD
# Meta platform        tcl
# Meta recommend       tcllibc
# Meta require         {Tcl 8.2}
# Meta require         msgcat
# Meta subject         ipv6 ip {rfc 3513} ipv4 {internet address}
# Meta summary         tcllib_ip
# @@ Meta End


if {![package vsatisfies [package provide Tcl] 8.2]} return

package ifneeded ip 1.2 [string map [list @ $dir] {
        # ACTIVESTATE TEAPOT-PKG BEGIN REQUIREMENTS

        package require Tcl 8.2
        package require msgcat

        # ACTIVESTATE TEAPOT-PKG END REQUIREMENTS

            source [file join {@} ip.tcl]

        # ACTIVESTATE TEAPOT-PKG BEGIN DECLARE

        package provide ip 1.2

        # ACTIVESTATE TEAPOT-PKG END DECLARE
    }]
