global tcl_platform
if { [string match "Linux" $tcl_platform(os)] && [string match "x86_64" $tcl_platform(machine)] } {
    package ifneeded tbcload 1.7 \
        [list load [file join $dir libtbcload1.7.so]]
}
