global tcl_platform
if { [string match "Linux" $tcl_platform(os)] && [string match "x86_64" $tcl_platform(machine)] } {
        package ifneeded tdom 0.8.3 \
                [list load [file join $dir libtdom0.8.3.so]]
}
