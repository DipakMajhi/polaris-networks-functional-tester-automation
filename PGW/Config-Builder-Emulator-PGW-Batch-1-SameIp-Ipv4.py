# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""


import Tkinter
import paramiko
import sys


commComm = 'comm::comm send [list '
hostName = sys.argv[1]        # Emulator IP Address : '192.168.233.133'
pgwPort = sys.argv[2]         # PGW Port : 5677
userName = 'root'             # Emulator username : root
passWord = 'polaris'          # Emulator password : polaris
emulatorStartIp = sys.argv[3] # Emulator Start IP Address : '192.168.233.100'



#Format : python    FileName.py     emulator_management_IP    PGW Port    emulator_start_IP

#----------- SSH Login to Emulator and starting up Nettest Server -------------
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostName, username=userName, password=passWord)

client.exec_command('cd /opt/polaris-nettest/server/; ./stop.sh')
stdin, stdout, stderr = client.exec_command('cd /opt/polaris-nettest/server/; ./start.sh -pgw '+pgwPort)
for line in stdout:
    print (line.strip('\n'))


client.close()




#--------------------- Assigning IP Addresses to all Nodes ----------------------
emulatorStartIp = emulatorStartIp.split('.')
baseIp = emulatorStartIp[0]+'.'+emulatorStartIp[1]+'.'+emulatorStartIp[2]+'.'
startIp = int(emulatorStartIp[3])
pgwIp = baseIp+str(startIp)
startIp+=1
pgwPcrfIp = baseIp+str(startIp)
startIp+=1
aaaIp = baseIp+str(startIp)
startIp+=1





#------------------API Calls to Emulator Server for Configuration--------------------
tkinterobj=Tkinter.Tk()
tkinterobj.tk.eval('source comm-package.tcl')

command = ['PNReset',
            '{PNConnect -statusUpdateHook :emulator:1 -statusUpdatePort 50671 -reset 0}',
            '{PNConfigureEventLogging -level Information -compression 0 -enableDevLog 0 -repetition 1 -size 20 -maxFileSize 10 -compressedFileCount 50}',
            '{PNCreateNetwork -plmn TestNetwork -mcc 001 -mnc 01}',
            '{PNPGWConfigureEmergencySession -enable 1  -apn emergency-apn.TestNetwork  -plmn TestNetwork -dhcpIPv4 192.168.233.111 -dhcpIPv6 ::1 -primaryDnsIPv4 8.8.8.8 -primaryDnsIPv6 1234::1 -secondaryDnsIPv4 0.0.0.0 -secondaryDnsIPv6 1234::0 -pcscfIPv4 192.168.0.1 -pcscfIPv6 ABCD::1 -dhcpIPv4AddressRangeStart 1.1.1.10 -dhcpIPv4AddressRangeEnd 1.1.1.20 -dhcpIPv6AddressPrefixStart 2100:0:1:: -dhcpIPv6AddressPrefixEnd 2100:0:64:: -interface ens33 -ueIPAddressRoutingPrefixSizeIPv4 20 -ueIPAddressRoutingPrefixSizeIPv6 48}',
            '{PNPGWCreatePDNProfile -plmn TestNetwork -apn apn.TestNetwork -pdnType 1 -userAuthenticatedByAAA 0 -subscriptRequired 0 -apnType 2 -dhcpIPv4 192.168.233.111 -primaryDnsIPv4 8.8.8.8 -ueIPAddressRoutingPrefixSizeIPv4 20}',
            '{PNPGWCreatePDNProfile -plmn TestNetwork -apn apn1.TestNetwork -pdnType 1 -userAuthenticatedByAAA 0 -subscriptRequired 0 -apnType 2 -ueIPAddressRoutingPrefixSizeIPv4 20}']

for i in range(0,len(command)):
  tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+command[i])
 
pgwHandle = tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWCreate -plmn TestNetwork -ipaddrS5c '+pgwIp+' -intfS5c ens33 -ipaddrS5c '+pgwIp+' -netmaskS5c 24 -portS5c 2123 -intfS5u ens33 -ipaddrS5u '+pgwIp+' -netmaskS5u 24 -portS5u 2152 -intfS8c ens33 -ipaddrS8c '+pgwIp+' -netmaskS8c 24 -portS8c 2123 -intfS8u ens33 -ipaddrS8u '+pgwIp+' -netmaskS8u 24 -portS8u 2152 -intfSGi ens33 -ipaddrSGi '+pgwIp+' -netmaskSGi 24 -intfS2Ac ens33 -ipaddrS2Ac '+pgwIp+' -netmaskS2Ac 24 -portS2Ac 2123 -intfS2Au ens33 -ipaddrS2Au '+pgwIp+' -netmaskS2Au 24 -portS2Au 2152 -intfS2Bc ens33 -ipaddrS2Bc '+pgwIp+' -netmaskS2Bc 24 -portS2Bc 2123 -intfS2Bu ens33 -ipaddrS2Bu '+pgwIp+' -netmaskS2Bu 24 -portS2Bu 2152 -intfS6b ens33 -ipaddrS6b '+pgwIp+' -netmaskS6b 24 -portS6b 3868 -intfGx ens33 -ipaddrGx '+pgwIp+' -netmaskGx 24 -portGx 3868 -diaHost "pgw1.localhost" -diaRealm "epc.mnc001.mcc001.3gppnetwork.org" -dhcpClientPort 67 -dhcpClientPortIPv6 546 -partialPathFailure 1 -nodeIdType 0 -nodeIdValue '+pgwIp+' -diaProxyPA2 0}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWConfigureDiameterAVPValidation -pgw '+str(pgwHandle)+' -isFailedAvpEnabled 0}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWUpdateConfiguration -pgw '+str(pgwHandle)+'  -enableLoopback 0  -enableFragmentation 1  -mtuSize 1400  -nonIpMtuSize 1358}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWSetBearerBindingPolicy -node '+str(pgwHandle)+'  -bearerBindingOptions 0}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWAddPDNProfile -pgw '+str(pgwHandle)+'  -apn apn.TestNetwork  -plmn TestNetwork}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWAddPDNProfile -pgw '+str(pgwHandle)+'  -apn apn1.TestNetwork  -plmn TestNetwork}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWConfigureNeighbourPCRF -pgw '+str(pgwHandle)+' -ipAddress '+pgwPcrfIp+' -diaPort 3868 -intfType 0  -isDefault 1 -trnsprtProto 6 -nodeType 1 -peerRealm epc.mnc001.mcc001.3gppnetwork.org}')

tkinterobj.tk.eval(commComm+str(pgwPort)+' '+hostName+'] '+'{PNPGWConfigureNeighbourAAA -pgw '+str(pgwHandle)+' -ipAddress '+aaaIp+' -diaPort 3868 -intfType 3  -isDefault 1 -trnsprtProto 6 -nodeType 1 -peerRealm epc.mnc001.mcc001.3gppnetwork.org}')
