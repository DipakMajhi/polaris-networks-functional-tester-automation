# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 20:02:33 2017

@author: Dipak Majhi
"""



import time
import subprocess
import sys

hostName = sys.argv[1]        # Emulator IP Address : '192.168.233.133'
pgwPort = sys.argv[2]         # PGW Port : 5677
start_ip = sys.argv[3]        # Emulator Start IP Address : '192.168.233.100'




#-----------Starting PolarisFT License Server-------------------
#subprocess.Popen(["service","polaris-license-server", "start"])


#------------------Starting PolarisFT Server---------------------
subprocess.Popen(["systemctl","stop", "polarisftd"])
subprocess.Popen(["systemctl","start", "polarisftd"])
time.sleep(2)
print (subprocess.Popen(["systemctl","status", "polarisftd"], stdout=subprocess.PIPE).communicate()[0])

#subprocess.Popen(["polarispgwft","-setup"])



#-------------------------------------------Executing Batch-1---------------------------------------------------

print ("\n\t\t --- : Executing Batch-1 ( Same IP - IPv4 ) : --- \n\n")


#-----------Output function to save the results in a text file for each Test Section or Test case-------------
def OutputBatch1SameIpIpv4(data, name):
  filename = str(name) +'.txt'
  with open('/home/polaris/PolarisFT-Automated-Testing/PGW/Batch-1/Same-Ip-Ipv4-Results/'+filename, 'w') as output:
    output.write(data)



#----------------Emulator (SPGW) Configuration File---------------
#print (subprocess.Popen(["polarispgwft","-setup"], stdout=subprocess.PIPE).communicate()[0])
subprocess.call(["python", "Config-Builder-Emulator-PGW-Batch-1-SameIp-Ipv4.py",hostName,pgwPort,start_ip])


#----------------PolarisFT (SPGW) Configuration File---------------
subprocess.call(["python", "Config-Builder-PolarisFT-PGW-Batch-1-SameIp-Ipv4.py",start_ip])




TC = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '29', '30', '31', '32',]

for i in range (0,len(TC)):
	data = (subprocess.Popen(["polarispgwft","-run", TC[i], "-v"], stdout=subprocess.PIPE).communicate()[0])
	print (data)
	OutputBatch1SameIpIpv4(data, ('Test-Section-'+str(TC[i])))
	subprocess.Popen(["systemctl","restart", "polarisftd"])
	time.sleep(1)







